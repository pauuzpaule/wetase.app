import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefs {
  SharedPreferences sharedPreferences;
  static final String TAG_FCM_TOKEN = "fcmToken";
  static final String TAG_USER_DATA = "userData";
  static final String TAG_LOGGED_IN_AS = "userType";
  SharedPrefs()  {
        () async{
      sharedPreferences = await SharedPreferences.getInstance();
    }();
  }

  removeAllData() async {
    SharedPreferences preferences =
    await SharedPreferences.getInstance();
    preferences.clear();
  }

  storeLoggedInAs(String type) async{
    sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString(TAG_LOGGED_IN_AS, type);
  }

  getLoggedInAs() async{
    sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString(TAG_LOGGED_IN_AS) == null ? null : sharedPreferences.getString(TAG_LOGGED_IN_AS);
  }

  storeFcmToken(var token) {
    sharedPreferences.setString(TAG_FCM_TOKEN, token);
  }

  getFcmToken() {
    return sharedPreferences.getString(TAG_FCM_TOKEN);
  }

  storeUser(var userJson) async {
    sharedPreferences = await SharedPreferences.getInstance();
    await sharedPreferences.setString(TAG_USER_DATA, userJson.toString());
  }

  getUser() async{
    sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString(TAG_USER_DATA) == null ? null : json.decode(sharedPreferences.getString(TAG_USER_DATA));
  }
}