import 'package:geocoder/geocoder.dart';
class Locator {
  geoDecode(lat, lng) async {
    final coordinates = new Coordinates(lat, lng);
    var addresses = await Geocoder.local.findAddressesFromCoordinates(coordinates);
    var first = addresses.first;
    return "${first.addressLine}";
  }
}