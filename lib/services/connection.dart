import 'dart:convert';
import 'package:http/http.dart' as http;

//final appurl = "http://192.168.1.106/agents2/public/";
//final appurl = "http://192.168.42.228/wetase/public/mobile";
final appurl = "https://wetase.sayrunjah.com/mobile";
makeCall(url) async {
  final _fullurl = appurl + url;
  final response = await http.get(_fullurl);
  return json.decode(response.body);
}

makePostCall(url, body) async {
  final _fullurl = appurl + url;
  var response = await http.post(_fullurl, body: body);
  return json.decode(response.body);
}


makeMultipartCall(url, body, file) async {
  final _fullurl = appurl + url;
  var response = await http.post(_fullurl, body: body);
  return json.decode(response.body);
}

makeSimpleRequest(url) async{
  final response = await http.get(url);
  return json.decode(response.body);
}