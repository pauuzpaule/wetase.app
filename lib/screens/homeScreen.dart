import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wetase/bloc/homeBloc.dart';
import 'package:wetase/bloc/state/homeState.dart';
import 'package:wetase/services/sharedPrefs.dart';

class HomeScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new HomeScreenState();
  }
}

class HomeScreenState extends State<HomeScreen> {
  final HomeBloc _homeBloc = HomeBloc();
  var _index = 1;
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Wetase"),
        actions: <Widget>[
          IconButton(
            onPressed: () {
              exitApp(context);
            },
            icon: Icon(Icons.exit_to_app,)
          )
        ],
      ),
      body: FutureBuilder(builder: (context, snapshot) {
        return BlocBuilder(
          bloc: _homeBloc,
          builder: (context, HomeState homeState) {
            return _homeBloc.displayCurrentView();
          },
        );
      }),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.contacts),
            title: Text('Contacts'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text('Home'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.list),
            title: Text('Alerts'),
          ),
        ],
        currentIndex: _index,
        selectedItemColor: Colors.amber[800],
        onTap: (index) {
          setState(() {
            _index = index;
          });
          _homeBloc.onChangeView(index);
        },
      ),
    );
  }

  exitApp(BuildContext context) async {
    final SharedPrefs _sharedPrefs = SharedPrefs();
    await _sharedPrefs.removeAllData();
    Navigator.pushReplacementNamed(context, "/login");

  }
}
