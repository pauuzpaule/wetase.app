import 'dart:convert';

import "package:flutter/material.dart";
import 'package:wetase/data/config.dart';
import 'package:wetase/services/connection.dart';
import 'package:wetase/util.dart';
import 'package:wetase/services/sharedPrefs.dart';

class SignUpScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return SignUpScreenState();
  }

}

class SignUpScreenState extends State <SignUpScreen> {

  final TextEditingController _name = TextEditingController();
  final TextEditingController _email = TextEditingController();
  final TextEditingController _phone = TextEditingController();
  final TextEditingController _password = TextEditingController();
  final TextEditingController _confirmPassword = TextEditingController();
  final SharedPrefs _sharedPrefs = SharedPrefs();


  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _name.dispose();
    _email.dispose();
    _phone.dispose();
    _password.dispose();
    _confirmPassword.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text(kAPPNAME),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(vertical: 20, horizontal: 10),
        child: Center(
          child: ListView(
            children: <Widget>[
              TextFormField(
                controller: _name,
                decoration: InputDecoration(
                    border: new OutlineInputBorder(
                      borderRadius: const BorderRadius.all(
                        const Radius.circular(10.0),
                      ),
                    ),
                    hintText: "Enter name"),
              ),
              SizedBox(height: 10,),
              TextFormField(
                controller: _email,
                decoration: InputDecoration(
                    border: new OutlineInputBorder(
                      borderRadius: const BorderRadius.all(
                        const Radius.circular(10.0),
                      ),
                    ),
                    hintText: "Enter email address"),
              ),
              SizedBox(height: 10,),
              TextFormField(
                controller: _phone,
                decoration: InputDecoration(
                    border: new OutlineInputBorder(
                      borderRadius: const BorderRadius.all(
                        const Radius.circular(10.0),
                      ),
                    ),
                    hintText: "Enter phone number"),
              ),
              SizedBox(height: 10,),
              TextFormField(
                controller: _password,
                obscureText: true,
                decoration: InputDecoration(
                    border: new OutlineInputBorder(
                      borderRadius: const BorderRadius.all(
                        const Radius.circular(10.0),
                      ),
                    ),
                    hintText: "Enter password"),
              ),
              SizedBox(height: 10,),
              TextFormField(
                controller: _confirmPassword,
                obscureText: true,
                decoration: InputDecoration(
                    border: new OutlineInputBorder(
                      borderRadius: const BorderRadius.all(
                        const Radius.circular(10.0),
                      ),
                    ),
                    hintText: "Confirm password"),
              ),
              SizedBox(height: 10,),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  RaisedButton(
                    padding: EdgeInsets.symmetric(vertical: 15.0),
                    child: Text("Login", style: TextStyle(color: Colors.white),),
                    color: Colors.blue[700],
                    onPressed: () async {
                      Navigator.pushNamed(context, "/login");
                    },
                  ),
                  SizedBox(width: 10.0,),
                  RaisedButton(
                    padding: EdgeInsets.symmetric(vertical: 15.0),
                    child: Text("Sign Up", style: TextStyle(color: Colors.white),),
                    color: Colors.blue[700],
                    onPressed: (){

                      if (!verifyText(_name, "Name") ||
                          !verifyText(_email, "Email") ||
                          !verifyText(_password, "Passoword")) {
                        return null;
                      }

                      if(_password.text != _confirmPassword.text) {
                        print(_password.text);
                        print(_confirmPassword.text);
                        showToast("Passwords don't match");
                        return null;
                      }

                        signUp(_name.text, _email.text, _password.text, _phone.text);
                    },
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  signUp(String name, String email, String password, String phone) async {
    var request = {
      "name" :name,
      "email": email,
      "password": password,
      "phone": phone,
    };
    var result = await makePostCall("/signUp", request);
    print(result);
    _sharedPrefs.storeUser(json.encode(result));
    Navigator.pushReplacementNamed(context, "/login");
  }
}