import 'package:flutter/material.dart';
import 'package:wetase/data/config.dart';
import 'package:wetase/widgets/roundImage.dart';

class ProfileScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new ProfileState();
  }

}

class ProfileState extends State<ProfileScreen> {
  @override
  Widget build(BuildContext context) {
    double screenHeight  = MediaQuery.of(context).size.height;

    // TODO: implement build
    return  Column(
        children: <Widget>[
          Container(
            height: screenHeight * .3,
            padding: EdgeInsets.only(top: 10.0),
            child: Center(
              child: Column(
                children: <Widget>[
                  RoundImage(),
                  Text(kTESTUSER.name),
                  Text(kTESTUSER.email)
                ],
              ),
            ),
          ),
          Container(
            height: screenHeight * .47,
          ),
        ],
      );
  }

}