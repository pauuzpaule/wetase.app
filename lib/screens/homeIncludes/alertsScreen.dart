import 'package:flutter/material.dart';
import 'package:wetase/screens/alertIncludes/placeScreen.dart';
import 'package:wetase/screens/alertIncludes/receivedScreen.dart';
import 'package:wetase/screens/alertIncludes/sentScreen.dart';
import 'package:wetase/screens/alertIncludes/trackScreen.dart';

class AlertsScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new AlertState();
  }
}

class AlertState extends State<AlertsScreen> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          flexibleSpace: SafeArea(
              child: TabBar(
            //isScrollable: true,
            labelPadding: EdgeInsets.all(10),
            tabs: [
              Tab(
                text: "SENT",
              ),
              Tab(
                text: "TRACK",
              ),
              Tab(
                text: "PLACES",
              ),
            ],
          )),
        ),
        body: TabBarView(
          children: [
            SentScreen(),
            TrackScreen(),
            PlaceScreen(),
          ],
        ),
      ),
    );
  }
}
