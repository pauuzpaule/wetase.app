import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:location/location.dart';
import 'package:wetase/bloc/homeBloc.dart';
import 'package:wetase/util.dart';

class IndexScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new IndexState();
  }
}

class IndexState extends State<IndexScreen> {
  final _homeBloc = HomeBloc();
  bool serviceRunning = false;
  final platform = const MethodChannel('com.sayrunjah.wetase');
  bool isSending = false;
  
  prepareApp(BuildContext context) async {
    await requestAllPermissions();

    final checkPermission = await hasPermission("location");
    if (!checkPermission) {
      var res;
      for(int i =0; i < 3; i++) {
        res = await requestPermission("location", context);
      }
      if (!res) {
        return null;
      }
    }

    return await Location().getLocation();
  }



  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: prepareApp(context),
        builder: (context, snapdata) {
          if (snapdata.connectionState == ConnectionState.done &&
              snapdata != null) {
            return Center(
                child: isSending ? CircularProgressIndicator() : Stack(
              children: <Widget>[

                Container(
                    width: 210.0,
                    height: 210.0,
                    decoration: new BoxDecoration(
                      color: Colors.white,
                      border:
                          new Border.all(color: Colors.redAccent, width: 2.0),
                      borderRadius: new BorderRadius.circular(210.0),
                    )),
                Positioned(
                  left: 5,
                  top: 5,
                  child: InkWell(
                    onTap: () {
                      onClickSend();
                    },
                    child: new Container(
                      width: 200.0,
                      height: 200.0,
                      decoration: new BoxDecoration(
                        color: Colors.redAccent,
                        border: new Border.all(color: Colors.white, width: 2.0),
                        borderRadius: new BorderRadius.circular(200.0),
                      ),
                      child: new Center(
                        child: new Text(
                          'ALERT !!',
                          style: new TextStyle(
                              fontSize: 18.0, color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                ),
                /*Positioned(
                  bottom: 0,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      RaisedButton(
                          child: Text("Start Service"), onPressed: () async {
                        try {
                          await platform.invokeMethod('start_service');
                        } on PlatformException catch (e) {
                          print("Platform Chanel error: '${e.message}'.");
                        }
                      }),
                      RaisedButton(
                          child: Text("Stop Service"), onPressed: () async {
                        try {
                          await platform.invokeMethod('stop_service');
                        } on PlatformException catch (e) {
                          print("Platform Chanel error: '${e.message}'.");
                        }
                      })
                    ],
                  ),
                ),*/
              ],
            ));
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        });
  }

  onClickSend() async {
    setState(() {
      isSending = true;
    });
    final res = await _homeBloc.sendAlert();
    setState(() {
      isSending = false;
    });
  }

  testChannel() {
    setState(() {

      int selection = 10 + (Random(1).nextInt(40-10));
      print("Okayyy ${selection.toString()}");
    });
  }
}
