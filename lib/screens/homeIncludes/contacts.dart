import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wetase/bloc/homeBloc.dart';
import 'package:wetase/bloc/state/homeState.dart';
import 'package:wetase/data/contact.dart';


class ContactScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new ContactScreenState();
  }
}

class ContactScreenState extends State<ContactScreen> {
  @override
  Widget build(BuildContext context) {
    final HomeBloc _homeBloc = HomeBloc();

    return FutureBuilder(
        future: _homeBloc.getContactsList(context),
        builder: (context, snapshot) {
          
          if ( (snapshot.connectionState == ConnectionState.done) && snapshot.data != null) {
            return BlocBuilder(
              bloc: _homeBloc,
              builder: (context, HomeState homeState) {
                return Scaffold(
                  appBar: AppBar(title: Text("Contacts"),),
                  body: ListView.builder(
                      itemCount: homeState.contactList.length,
                      itemBuilder: (context, i) {
                        MyContact contact = homeState.contactList[i];
                        return ContactItem(contact: contact,);
                      }),
                  floatingActionButton: FloatingActionButton(
                      child: Icon(Icons.save),
                      onPressed: () async{
                        _homeBloc.saveContacts(context);
                      }),
                );
              },
            );
          } else {
            return Scaffold(
              appBar: AppBar(title: Text("Contacts"),),
              body: Center(child: CircularProgressIndicator()),
            );
          }
        });
  }
}

class ContactItem extends StatefulWidget {
  final contact;
  final index;
  final homeBloc;
  ContactItem({this.contact, this.index, this.homeBloc});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ContactItemState(contact: contact);
  }

}

class ContactItemState extends State<ContactItem> {
  final index;
  final homeBloc;
  final contact;
  ContactItemState({this.contact, this.index, this.homeBloc});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Card(
      child: ListTile(
        leading: Icon(
          Icons.contact_phone,
          size: 32.0,
        ),
        title: Text(contact.name),
        subtitle: Text(contact.phoneNumber),
        trailing: Checkbox(value: contact.isInList, onChanged: (bool value){
          setState(() {
            contact.isInList = value;
          });
        }),
      ),
    );
  }

}
