import 'package:flutter/material.dart';
import 'package:wetase/bloc/homeBloc.dart';
import 'package:wetase/data/contact.dart';


class MyContactsScreen  extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new MyContactsScreenState();
  }

}

class MyContactsScreenState extends State<MyContactsScreen> {

  final HomeBloc _homeBloc = HomeBloc();

  @override
  Widget build(BuildContext context) {

    return FutureBuilder(
        future: _homeBloc.getContacts(),
        builder: (context, snapdata) {
      if (snapdata.connectionState == ConnectionState.done && snapdata.data != null) {
        List _contactList = snapdata.data;
        // TODO: implement build
        return Stack(
          children: <Widget>[
            SizedBox.expand(
              child: _contactList.length > 0 ? ListView.builder(
                  itemCount: _contactList.length,
                  itemBuilder: (context, i) {
                    MyContact contact = _contactList[i];
                    return Dismissible(
                      // Show a red background as the item is swiped away.
                      background: Container(color: Colors.red),
                      key: Key(contact.phoneNumber),
                      onDismissed: (direction) async {
                        setState(() {
                          _contactList.removeAt(i);
                        });


                        Scaffold
                            .of(context)
                            .showSnackBar(SnackBar(content: Text(contact.name+" removed")));
                        _contactList = await _homeBloc.deleteContact(contact.id.toString());
                      },
                      child: Card(
                        child: ListTile(
                            leading: Icon(
                              Icons.contact_phone,
                              size: 32.0,
                            ),
                            title: Text(contact.name),
                            subtitle: Text(contact.phoneNumber)),
                      ),
                    );

                  }) : Center(child: Text("No Contacts ):"),),
            ),
            Positioned(
              bottom: 10,
              right: 10,
              child: FloatingActionButton(
                  child: Icon(Icons.add),
                  onPressed: () {
                    Navigator.pushNamed(context, '/contacts');
                  }),
            ),
          ],
        );
      }else {
        return Center(child: CircularProgressIndicator(),);
      }
    });
  }

}