import 'package:flutter/material.dart';

class ReceivedScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new ReceivedScreenState();
  }

}

class ReceivedScreenState extends State<ReceivedScreen> {
  @override
  Widget build(BuildContext context) {

    return FutureBuilder(
      builder: (context, snapdata) {
        return SizedBox.expand(
          child: ListView.builder(
            itemCount: 20,
            itemBuilder: (context, i) {
              return  Card(
                child: ListTile(
                    leading: Icon(
                      Icons.error,
                      size: 32.0,
                      color: Colors.redAccent,
                    ),
                    title: Text("Sayrunjah Pauuz"),
                    subtitle: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text("Naguru katalima"),
                        SizedBox(width: 10,),
                        Text("12/Jun/2019"),
                      ],
                    )
                ),
              );
            },
          ),
        );
      },
    );
  }
}