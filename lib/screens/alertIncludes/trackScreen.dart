import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:location/location.dart';
import 'package:sms/sms.dart';
import 'package:wetase/data/config.dart';
import 'package:wetase/data/contact.dart';
import 'package:wetase/data/trackMessage.dart';
import 'package:wetase/services/dbHelper.dart';
import 'package:wetase/services/locator.dart';
import 'package:wetase/services/sharedPrefs.dart';

class TrackScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new TrackScreenState();
  }
}

class TrackScreenState extends State<TrackScreen> {
  final platform = const MethodChannel('com.sayrunjah.wetase');
  var db = DBHelper();
  final TextEditingController _depat = TextEditingController();
  final TextEditingController _destination = TextEditingController();
  final TextEditingController _time = TextEditingController();
  List _trackList;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    platform.setMethodCallHandler((call) {
      if (call.method == 'run') {
          startTracking();
      }
      return null;
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _destination.dispose();
    _depat.dispose();
    _time.dispose();
  }

  getTrackList() async {
    List trackList = [];
    var list = await db.getAll("Tracks");
    print(list);
    for(int i = 0; i < list.length; i++) {
      trackList.add(TrackMessage(
        id: list[i]["id"],
        departure: list[i]["depature"],
        destination: list[i]["destination"]
      ));
    }
    return trackList;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: getTrackList(),
      builder: (context, snapdata) {
        if(snapdata.connectionState == ConnectionState.done && snapdata.data != null) {
          _trackList = snapdata.data;
          return Stack(
            children: <Widget>[
              SizedBox.expand(
                child: ListView.builder(
                  itemCount: _trackList.length,
                  itemBuilder: (context, i) {
                    TrackMessage _trackMessage = _trackList[i];
                    return Dismissible(
                      background: Container(color: Colors.red),
                      key: Key(_trackMessage.id.toString()),
                      onDismissed: (direction) async {
                        stopService();
                        Scaffold
                            .of(context)
                            .showSnackBar(SnackBar(content: Text("Tracking stopped")));
                      },
                      child: Card(
                        child: ListTile(
                            dense: true,
                            leading: Icon(
                              Icons.transfer_within_a_station,
                              size: 32.0,
                              color: Colors.green,
                            ),
                            title: Text("${_trackMessage.departure} - ${_trackMessage.destination}"),
                            ),
                      ),
                    );
                  },
                ),
              ),
              Positioned(
                bottom: 10,
                right: 10,
                child: FloatingActionButton(
                    child: Icon(Icons.add),
                    onPressed: () {
                      _showDialog();
                    }),
              ),
            ],
          );
        }else {
          return Center(child: CircularProgressIndicator(),);
        }
      },
    );
  }



  void _showDialog() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Tracking Information"),
          content: Container(
            height: MediaQuery.of(context).size.height * .3,
            child: ListView(
              children: <Widget>[
                TextFormField(
                  controller: _depat,
                  decoration: InputDecoration(labelText: 'Enter your depature'),
                ),
                TextFormField(
                  controller: _destination,
                  decoration:
                      InputDecoration(labelText: 'Enter your destination'),
                ),
                TextFormField(
                  controller: _time,
                  keyboardType: TextInputType.number,
                  decoration:
                      InputDecoration(labelText: 'Time interval (mins)'),
                ),
              ],
            ),
          ),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Ok"),
              onPressed: () {
                if (!verifyText(_depat, "Depature") ||
                    !verifyText(_destination, "Destination") ||
                    !verifyText(_time, "Time interver")) {
                  return null;
                }

                createTracks(_depat.text, _destination.text, _time.text);
                startService(int.parse(_time.text));
                Navigator.of(context).pop();
              },
            ),
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                _destination.dispose();
                _depat.dispose();
                _time.dispose();
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  verifyText(TextEditingController textEditingController, String name) {
    if (textEditingController.text.length == 0) {
      Fluttertoast.showToast(
          msg: "$name is required",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
      return false;
    }
    return true;
  }

  createTracks(dept, dest, time) async {
    final fields = "depature, destination, time";
    final values = "'$dept' ,'$dest', '$time'";
    await db.insert("Tracks", fields, values);
    setState(() {
      // ignore: unnecessary_statements
      (() async {
        // ignore: unnecessary_statements
        _trackList = await getTrackList();
      });
    });
  }

  startService(time) async{
    try {
      await platform.invokeMethod('start_service', {"time": time});
    } on PlatformException catch (e) {
      print("Platform Chanel error: '${e.message}'.");
    }
  }

  stopService() async{
    try {
      await platform.invokeMethod('stop_service');
    } on PlatformException catch (e) {
      print("Platform Chanel error: '${e.message}'.");
    }
  }

  getContactDB() async {
    final db = DBHelper();
    return await db.getAll("MyContacts");
  }

  getContacts() async {
    var list = await getContactDB();
    List myContacts = [];
    for (var i = 0; i < list.length; i++) {
      myContacts.add(MyContact(
          id: list[i]["id"],
          name: list[i]["name"],
          phoneNumber: list[i]["number"]));
    }

    return myContacts;
  }

  startTracking() async {
    final SharedPrefs _sharedPrefs = SharedPrefs();
    var user = await _sharedPrefs.getUser();

    final track = await db.lastInsert("Tracks");
    var location = await Location().getLocation();
    String msg = "Location update of: ${user['name']}";
    msg += "\n From ${track[0]["depature"]} to ${track[0]["destination"]}";
    msg += "\n Cordinates: ${location['latitude'].toString()} , ${location['longitude'].toString()}";
    //print(msg);
    var placeName = "";
    try {
      placeName = await Locator()
          .geoDecode(location['latitude'], location['longitude']);
      msg += "\n ${placeName.toString()}";
    }catch(e) {

    }

    List contacts = await getContacts();
    if(contacts.length == 0) {
      return "NO_CONTACTS";
    }

    for(int i =0; i < contacts.length; i++) {
      MyContact myContact = contacts[i];
      try {
        sendSms(myContact.phoneNumber, msg);
      }catch(e) {
        print(e);
      }
    }
    print(msg);
  }

  sendSms(address, text) {
    SmsSender sender = new SmsSender();
    SmsMessage message = new SmsMessage(address, text);
    message.onStateChanged.listen((state) {
      if (state == SmsMessageState.Sent) {
        print("$address SMS is sent!");
      } else if (state == SmsMessageState.Delivered) {
        print("$address SMS is delivered!");
      }else if(state == SmsMessageState.Fail) {
      }
    });
    sender.sendSms(message);
  }
}
