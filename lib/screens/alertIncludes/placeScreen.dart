import 'package:flutter/material.dart';
import 'package:location/location.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:wetase/data/place.dart';
import 'package:wetase/services/connection.dart';
import 'package:wetase/util.dart';

class PlaceScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new PlaceScreenState();
  }
}

class PlaceScreenState extends State<PlaceScreen> {
  getPlacesNearBy() async {
    List placeList = [];

    final checkPermission = await hasPermission("location");
    if (!checkPermission) {
      final res = await requestPermission("location", context);
      if (!res) {
        return null;
      }
    }

    var location = await Location().getLocation();
    String loc = "${location["latitude"]},${location["longitude"]}";
    print(loc);

    String url =
        "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$loc&radius=5000&keyword=hospital|police&key=AIzaSyB2Z31DSOmOapplyKPknJMSRDFYm9tAlCw";
    var results = await makeSimpleRequest(url);
    var data = results['results'];
    for(int i =0; i < data.length; i++) {
      String name = data[i]["name"];
      String type = data[i]["types"][0];
      var geometry = data[i]["geometry"]["location"];
      placeList.add(Place(
        name: name, type: type, lat: geometry["lat"], lng: geometry["lng"]
      ));
    }

    return placeList;
  }

  openMap(double latitude, double longitude) async {
    String googleUrl = 'https://www.google.com/maps/search/?api=1&query=$latitude,$longitude';
    if (await canLaunch(googleUrl)) {
      await launch(googleUrl);
    } else {
      throw 'Could not open the map.';
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: getPlacesNearBy(),
      builder: (context, snapdata) {
        if (snapdata.connectionState == ConnectionState.done &&
            snapdata.data != null) {
          List placeList = snapdata.data;
          return SizedBox.expand(
            child: ListView.builder(
              itemCount: placeList.length,
              itemBuilder: (context, i) {
                Place place = placeList[i];
                return Card(
                  child: InkWell(
                    onTap: () async {
                      openMap(place.lat, place.lng);
                    },
                    child: ListTile(
                        leading: Icon(
                          Icons.place,
                          size: 32.0,
                          color: Colors.green,
                        ),
                        title: Text(place.name),
                        subtitle: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[Text(place.type)],
                        ),
                      trailing: InkWell(
                        child: Icon(Icons.arrow_forward_ios),
                      ),
                    ),
                  ),
                );
              },
            ),
          );
        } else {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }
}
