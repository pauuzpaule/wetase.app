import 'package:flutter/material.dart';
import 'package:wetase/data/alertMessage.dart';
import 'package:wetase/services/dbHelper.dart';
import 'package:wetase/util.dart';

class SentScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new SentScreenState();
  }

}

class SentScreenState extends State<SentScreen> {

  final db = DBHelper();


  getAlertsList() async {
    List alertList = [];
    final res = await db.getAll("Alerts");
    for(int i =0; i < res.length; i++) {
      alertList.add(AlertMessage(
        id: res[i]["id"],
        location: res[i]["location"],
        lat: res[i]["lat"],
        lng: res[i]["lng"],
        time: res[i]["time"],
      ));
    }
    return alertList;
  }

  @override
  Widget build(BuildContext context) {

    return FutureBuilder(
      future: getAlertsList(),
      builder: (context, snapdata) {
        if(snapdata.connectionState == ConnectionState.done && snapdata.data != null) {
          List alertList = snapdata.data;
          return SizedBox.expand(
            child: ListView.builder(
              itemCount: alertList.length,
              itemBuilder: (context, i) {
                AlertMessage alertMessage = alertList[i];
                return  Card(
                  child: ListTile(
                      leading: Icon(
                        Icons.info,
                        size: 32.0,
                        color: Colors.redAccent,
                      ),
                      title: Text("${formatDate(alertMessage.time, "E dd/MM/y kk:mm:ss")}"),
                      subtitle: Text("${alertMessage.location}")
                  ),
                );
              },
            ),
          );
        }else {
          return Center(child: CircularProgressIndicator(),);
        }
      },
    );
  }
}