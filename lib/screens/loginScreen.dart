import 'dart:convert';

import "package:flutter/material.dart";
import 'package:wetase/data/config.dart';
import 'package:wetase/services/connection.dart';
import 'package:wetase/services/sharedPrefs.dart';

import '../util.dart';

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return LoginScreenState();
  }

}

class LoginScreenState extends State <LoginScreen> {

  final TextEditingController _email = TextEditingController();
  final TextEditingController _password = TextEditingController();
  final SharedPrefs _sharedPrefs = SharedPrefs();


  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _email.dispose();
    _password.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text(kAPPNAME),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(vertical: 20, horizontal: 10),
        child: Center(
          child: ListView(
            children: <Widget>[
              TextFormField(
                controller: _email,
                decoration: InputDecoration(
                    border: new OutlineInputBorder(
                      borderRadius: const BorderRadius.all(
                        const Radius.circular(10.0),
                      ),
                    ),
                    hintText: "Enter email address"),
              ),
              SizedBox(height: 20.0,),
              TextFormField(
                controller: _password,
                decoration:
                InputDecoration(
                    border: new OutlineInputBorder(
                      borderRadius: const BorderRadius.all(
                        const Radius.circular(10.0),
                      ),
                    ),
                    hintText: "Enter password"),
              ),
              SizedBox(height: 20.0,),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  RaisedButton(
                    padding: EdgeInsets.symmetric(vertical: 15.0),
                    child: Text("Login",style: TextStyle(color: Colors.white),),
                    color: Colors.blue[700],
                    onPressed: () async {
                      var request = {
                        "email": _email.text,
                        "password": _password.text,
                      };
                      var result = await makePostCall("/loginUser", request);
                      if (result["status"] != null && result["status"] == "fail") {
                        showToast("Wrong login combination");
                      } else {
                        var userData = json.encode(result);
                        await _sharedPrefs.storeUser(userData);
                        Navigator.pushReplacementNamed(context, '/home');
                      }
                    },
                  ),
                  SizedBox(width: 10.0,),
                  RaisedButton(
                    padding: EdgeInsets.symmetric(vertical: 15.0),
                    child: Text("Sign Up", style: TextStyle(color: Colors.white),),
                    color: Colors.blue[700],
                    onPressed: (){
                      Navigator.pushNamed(context, "/signUp");
                    },
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }


}