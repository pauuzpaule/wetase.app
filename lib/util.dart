import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:permission/permission.dart';

formatPhoneNumber(number){
  RegExp regExp = new RegExp(r"[a-zA-Z\s-()+]");
  var replaced = number.replaceAll(regExp,'');
  RegExp reg256 = new RegExp(r"^256");
  return (replaced.substring(0,3) == "256") ? replaced.replaceAll(reg256, "0") : replaced;
}

formatDate(String date, format) {
  return DateFormat(format).format(DateTime.parse(date));
}

verifyText(TextEditingController textEditingController, String name) {
  if (textEditingController.text.length == 0) {
    Fluttertoast.showToast(
        msg: "$name is required",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIos: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0);
    return false;
  }
  return true;
}

showToast(String message) {
  Fluttertoast.showToast(
      msg: "$message",
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.CENTER,
      timeInSecForIos: 1,
      backgroundColor: Colors.red,
      textColor: Colors.white,
      fontSize: 16.0);
}

Future<bool> hasPermission(requestedPermission) async {
  var allPermissions = {
    "location" : PermissionName.Location,
    "contacts" : PermissionName.Contacts,
    "storage" : PermissionName.Storage,
    "sms" : PermissionName.SMS,
    "phone" : PermissionName.Phone
  };
  var hasPermission = false;
  List<Permissions> permissions =
  await Permission.getPermissionStatus([allPermissions[requestedPermission]]);
  permissions.forEach((permission) {
    if (permission.permissionStatus.toString() == "PermissionStatus.allow") {
      hasPermission = true;
    }
  });

  return hasPermission;
}

Future<bool> requestPermission(requestedPermission, context) async {
  var allPermissions = {
    "location" : PermissionName.Location,
    "contacts" : PermissionName.Contacts,
    "storage" : PermissionName.Storage,
    "sms" : PermissionName.SMS,
    "phone" : PermissionName.Phone
  };

  final res = await Permission.requestSinglePermission(allPermissions[requestedPermission]);
  return res.toString() == "PermissionStatus.allow";
}

 requestAllPermissions() async {

  List<PermissionName> permissionList = [];
  permissionList.add(PermissionName.Location);
  permissionList.add(PermissionName.Storage);
  permissionList.add(PermissionName.Phone);
  permissionList.add(PermissionName.SMS);
  permissionList.add(PermissionName.Contacts);
  final res = await Permission.requestPermissions(permissionList);
  return res;
}