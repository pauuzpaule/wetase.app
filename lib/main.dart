import 'package:flutter/material.dart';
import 'package:wetase/screens/homeIncludes/contacts.dart';
import 'package:wetase/screens/homeScreen.dart';
import 'package:wetase/screens/loginScreen.dart';
import 'package:wetase/screens/signUpScreen.dart';
import 'package:wetase/services/sharedPrefs.dart';

import 'data/config.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: kAPPNAME,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      routes: <String, WidgetBuilder>{
        "/home": (context) => HomeScreen(),
        "/contacts" : (context) => ContactScreen(),
        "/login" : (context) => LoginScreen(),
        "/signUp" : (context) => SignUpScreen(),
      },
      home: FutureBuilder(
          future: loginStatus(),
          builder: (context, snapdata){
            if(snapdata.connectionState == ConnectionState.done) {
              if(snapdata.data != null) {
                return HomeScreen();
              }else {
                return LoginScreen();
              }
            }else {
              return Scaffold(
                body: Center(child: CircularProgressIndicator(),),
              );
            }
          }),
    );
  }

  loginStatus() async{
    final SharedPrefs _sharedPrefs = SharedPrefs();
     return await _sharedPrefs.getUser();
  }
}
