import 'package:flutter/material.dart';

class RoundImage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Stack(
      children: <Widget>[
        Container(
            width: 100.0,
            height: 100.0,
            decoration: new BoxDecoration(
              color: Colors.white,
              border: new Border.all(color: Colors.redAccent, width: 2.0),
              borderRadius: new BorderRadius.circular(210.0),
            )),
        Positioned(
          left: 5,
          top: 5,
          child: InkWell(
            child: Container(
                width: 90.0,
                height: 90.0,
                decoration: new BoxDecoration(
                    shape: BoxShape.circle,
                    image: new DecorationImage(
                        fit: BoxFit.fill,
                        image: new NetworkImage(
                            "https://i.imgur.com/BoN9kdC.png")
                    )
                )),
          ),
        )
      ],
    );
  }
}
