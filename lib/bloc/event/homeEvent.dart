abstract class HomeEvent{}

class ChangeView extends HomeEvent{}

class GetContacts extends HomeEvent{}

class SendAlert extends HomeEvent{}