class HomeState {
  int viewIndex;
  List contactList;
  List selectedContacts;
  HomeState({this.viewIndex, this.contactList, this.selectedContacts});

  factory HomeState.init() => HomeState(viewIndex: 1, contactList: [], selectedContacts: []);

}
