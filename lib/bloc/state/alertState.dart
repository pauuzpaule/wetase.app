class AlertState {
  int viewIndex;
  List sentAlerts;
  List receivedAlerts;
  List tracks;
  List places;

  AlertState({this.viewIndex, this.sentAlerts, this.receivedAlerts, this.tracks, this.places});

  factory AlertState.init() => AlertState(viewIndex: 1,sentAlerts: [], receivedAlerts: [], tracks: [], places: []);
}