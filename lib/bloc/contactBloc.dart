import 'package:bloc/bloc.dart';
import 'package:wetase/bloc/event/contactEvent.dart';
import 'package:wetase/bloc/state/contactState.dart';



class ContactBloc extends Bloc<ContactEvent, ContactState> {
  @override
  // TODO: implement initialState
  ContactState get initialState => ContactState.init();

  @override
  Stream <ContactState> mapEventToState(ContactEvent event) async* {
    // TODO: implement mapEventToState
    if(event is GetContactList) {
      yield ContactState();
    }
  }

  getContactList(int currentView) {
    dispatch(GetContactList());
  }


}