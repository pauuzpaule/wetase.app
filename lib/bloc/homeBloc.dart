import 'package:bloc/bloc.dart';
import 'package:contacts_service/contacts_service.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:location/location.dart';
import 'package:sms/sms.dart';
import 'package:wetase/bloc/event/homeEvent.dart';
import 'package:wetase/bloc/state/homeState.dart';
import 'package:wetase/data/config.dart';
import 'package:wetase/data/contact.dart';
import 'package:wetase/screens/homeIncludes/alertsScreen.dart';
import 'package:wetase/screens/homeIncludes/indexScreen.dart';
import 'package:wetase/screens/homeIncludes/myContactsScreen.dart';
import 'package:wetase/services/connection.dart';
import 'package:wetase/services/dbHelper.dart';
import 'package:wetase/services/locator.dart';
import 'package:wetase/services/sharedPrefs.dart';

import '../util.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  BuildContext context;
  var db = DBHelper();

  @override
  // TODO: implement initialState
  HomeState get initialState => HomeState.init();

  @override
  Stream<HomeState> mapEventToState(HomeEvent event) async* {
    // TODO: implement mapEventToState
    if (event is ChangeView) {
      yield HomeState(viewIndex: this.currentState.viewIndex);
    }

    if (event is GetContacts) {
      yield HomeState(contactList: this.currentState.contactList);
    }

    if (event is SendAlert) {
      yield HomeState();
    }
  }

  onChangeView(int currentView) {
    this.currentState.viewIndex = currentView;
    dispatch(ChangeView());
  }

  onGetContacts(BuildContext context) async {
    this.currentState.contactList = await getContactsList(context);
    dispatch(GetContacts());
  }

  displayCurrentView() {
    switch (this.currentState.viewIndex) {
      case 0:
        return MyContactsScreen();
      case 1:
        return IndexScreen();
      default:
        return AlertsScreen();
    }
  }

  getContactsList(context) async {
    // Get all contacts
    final checkPermission = await hasPermission("contacts");
    if (!checkPermission) {
      final res = await requestPermission("contacts", context);
      if (!res) {
        return null;
      }
    }

    await getContactDB();
    if (this.currentState.contactList.length > 0) {
      return this.currentState.contactList;
    }

    Iterable<Contact> contacts = await ContactsService.getContacts();
    List myList = [];
    for (Contact c in contacts) {
      final phone = (c.phones != null && c.phones.length > 0
          ? formatPhoneNumber(c.phones.first.value)
          : "No Phone number");
      var myContact = new MyContact(
          name: c.displayName, phoneNumber: phone, isInList: false);
      //print("Heloooo");
      checkIfInList(myContact, this.currentState.selectedContacts);
      myList.add(myContact);
    }

    this.currentState.contactList = myList;
    return myList;
  }

  checkContact(int index, bool value) {
    MyContact _mycontact = this.currentState.contactList[index];
    _mycontact.isInList = value;
  }

  getContactDB() async {
    final db = DBHelper();
    this.currentState.selectedContacts = await db.getAll("MyContacts");
    return this.currentState.selectedContacts;
  }

  checkIfInList(MyContact contact, list) {
    for (var i = 0; i < list.length; i++) {
      if ((contact.name == list[i]["name"] &&
          contact.phoneNumber == list[i]["number"])) {
        contact.isInList = true;
      }
    }
  }

  saveContacts(BuildContext context) async {
    final db = new DBHelper();
    await db.deleteAll("MyContacts");
    final fields = "name, number";
    for (MyContact myContact in this.currentState.contactList) {
      if (myContact.isInList) {
        final values =
            "'" + myContact.name + "'," + "'" + myContact.phoneNumber + "'";
        await db.insert("MyContacts", fields, values);
      }
    }
    Navigator.pop(context);
  }

  getContacts() async {
    var list = await getContactDB();
    List myContacts = [];
    for (var i = 0; i < list.length; i++) {
      myContacts.add(MyContact(
          id: list[i]["id"],
          name: list[i]["name"],
          phoneNumber: list[i]["number"]));
    }

    return myContacts;
  }

  deleteContact(id) async {
    final db = new DBHelper();
    await db.delete("MyContacts", id);
    return getContacts();
  }

  sendAlert() async {
    final checkPermission = await hasPermission("location");
    if (!checkPermission) {
      final res = await requestPermission("location", context);
      if (!res) {
        return null;
      }
    }

    final SharedPrefs _sharedPrefs = SharedPrefs();
    var user = await _sharedPrefs.getUser();

    var location = await Location().getLocation();
    String msg = "SOS: ${user['name']} needs help ASAP reach out on ${user['phone']}";
    msg += "\n Cordinates: ${location['latitude'].toString()} , ${location['longitude'].toString()}";
    //print(msg);
    var placeName = "";
    try {
      placeName = await Locator()
          .geoDecode(location['latitude'], location['longitude']);
      msg += "\n ${placeName.toString()}";
    }catch(e) {

    }
    DateTime now = DateTime.now();
    String formattedDate2 = DateFormat('y-MM-dd kk:mm:ss').format(now);
    print(formattedDate2);
    print(msg);


    await saveAlertMessage(placeName, location['latitude'].toString(), location['longitude'].toString(), formattedDate2);
    var lastInsert = await db.lastInsert("Alerts");

    List contacts = await getContacts();
    if(contacts.length == 0) {
      return "NO_CONTACTS";
    }

    for(int i =0; i < contacts.length; i++) {
      MyContact myContact = contacts[i];
      try {
        sendSms(myContact.phoneNumber, msg, lastInsert[0]["id"]);
      }catch(e) {
        print(e);
      }
    }

    try {

      var request = {
        "user_id": user["id"].toString(),
        "lat": location['latitude'].toString(),
        "lng": location['longitude'].toString(),
        "sent_at": formattedDate2
      };
      var result = await makePostCall("/sendAlert", request);
      print(result);

    }catch(e) {
      print(e);
    }

    return "DONE";
  }

  sendSms(address, text, alertId) {
    SmsSender sender = new SmsSender();
    SmsMessage message = new SmsMessage(address, text);
    message.onStateChanged.listen((state) {
      if (state == SmsMessageState.Sent) {
        saveSentTo(alertId, address, "SENT");
        print("$address SMS is sent!");
      } else if (state == SmsMessageState.Delivered) {
        print("$address SMS is delivered!");
      }else if(state == SmsMessageState.Fail) {
        saveSentTo(alertId, address, "Fail");
      }
    });
    sender.sendSms(message);
  }

  saveAlertMessage(location, lat, lng, time) async {
    final fields = "location, lat, lng, time";
    final values = "'$location' ,'$lat','$lng', '$time'";
    await db.insert("Alerts", fields, values);
  }

  saveSentTo(alertId, number, status) async {
    final fields = "alert_id, number, status";
    final values = "'$alertId' ,'$number', '$status'";
    await db.insert("SentTo", fields, values);
  }
}
