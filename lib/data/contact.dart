class MyContact {
  int id;
  String name;
  String phoneNumber;
  bool isInList = false;

  MyContact({this.id, this.name, this.phoneNumber, this.isInList});

  dummyList() {
    List<MyContact> _contactList = [];
    for (int i = 0; i < 20; i++) {
      _contactList.add(
          MyContact(id: i, name: "Sayrunjah Pauuz", phoneNumber: "0752370787"));
    }
    return _contactList;
  }
}
