class Place {
  String name;
  String type;
  double lat;
  double lng;

  Place({this.name, this.type, this.lat, this.lng});
}