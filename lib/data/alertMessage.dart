class AlertMessage {
  int id;
  String location;
  String time;
  String lat;
  String lng;

  AlertMessage({this.id, this.location, this.time, this.lat, this.lng});
}