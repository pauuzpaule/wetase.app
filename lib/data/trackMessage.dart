class TrackMessage {
  int id;
  String departure;
  String destination;
  String lat;
  String lng;

  TrackMessage({this.id, this.departure, this.destination, this.lat, this.lng});
}