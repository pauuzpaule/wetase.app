package com.sayrunjah.wetase.wetase;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import io.flutter.app.FlutterActivity;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugins.GeneratedPluginRegistrant;

public class MainActivity extends FlutterActivity {
  private static final String WETASE_CHANNEL = "com.sayrunjah.wetase";
  public static MethodChannel METHOD_CHANNEL;
  public static int TIME_INTERVAL;
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    GeneratedPluginRegistrant.registerWith(this);
    MethodChannel methodChannel = new MethodChannel(getFlutterView(), WETASE_CHANNEL);
      METHOD_CHANNEL = methodChannel;

    methodChannel.setMethodCallHandler(
            (call, result) -> {
              if(call.method.equals("start_service")) {
                  TIME_INTERVAL = call.argument("time");
                  Toast.makeText(this, "Service started by user. "+String.valueOf(TIME_INTERVAL), Toast.LENGTH_LONG).show();
                  startService(new Intent(MainActivity.this,TrackerService.class));
              }
              if(call.method.equals("stop_service")) {
                  stopService(new Intent(MainActivity.this,TrackerService.class));
              }
            });

  }
}
