package com.sayrunjah.wetase.wetase;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.widget.Toast;

public class TrackerService extends Service {

    private final int TIME_INTERVAL = 5 * 1000;
    Runnable runnable;
    Handler handler;
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        handler = new Handler();
        runnable = () -> {
            MainActivity.METHOD_CHANNEL.invokeMethod("run", "");
            Toast.makeText(this, "Service running. "+String.valueOf(MainActivity.TIME_INTERVAL), Toast.LENGTH_SHORT).show();
            handler.postDelayed(runnable, MainActivity.TIME_INTERVAL * 60 * 1000);
        };
        handler.postDelayed(runnable, MainActivity.TIME_INTERVAL * 60 * 1000);
        return START_STICKY;
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        //Toast.makeText(this, "Service destroyed by user.", Toast.LENGTH_LONG).show();
        handler.removeCallbacks(runnable);
        stopSelf();
    }
}
